// vitals
PulseOximeter pox;

// gps
Adafruit_GPS GPS;


void setup() {
  // vitals
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  // temperature sensor
  pinMode(A1, INPUT);

  // humidity
  pinMode(6, OUTPUT);

  // gps
  pinMode(7, OUTPUT);

  Serial.begin(9600);
}
 
void loop() {
  // vitals
  if (Serial.available() > 0) {
      if (pox.begin()) {
          string spO2_res = pox.getSpO2();
          string hb_res = pox.getHeartRate();
          Serial.println("this is oxygen percentage: " + spO2_res + ". this is heart rate: " + hb_res);
      }
      digitalWrite(3, HIGH);
  } 
  else
  {
      Serial.println("No vitals data");
      digitalWrite(3, LOW);
  }

  // temperature
  float temp = analogRead(A1) / 1023.0 * 5.0 * 100.0;
  Serial.println("Temperature: " + to_string(temp));

  // humidity
  float h = digitalRead(7);
  Serial.println("Humidity: " + to_string(h));


  // gps
  if (Serial.available() > 0) {
      string gpsResult = GPS.read();
      Serial.println("gps coordinates: " + gpsResult);
  } else {
      Serial.println("No gps data");
  }

  Serial.println("");

  delay(100);
}
